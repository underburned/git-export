# Git export

Export data and metadata from GitLab:
- groups and subgroups
- personal projects
- project's wiki pages

# Requirements
[Personal access token](https://gitlab.com/-/profile/personal_access_tokens) with `api`, `read_api`, `read_repository`
checked.

Set `dir_path`. Put your username and token in `token.txt` or set `user_name` and `private_token` manually.

Run git_export.py.

> All operations with accessible data are read-only except 
> [global notification level setting](https://gitlab.com/-/profile/notifications).
> This level is set to `Disabled` before exporting (muting email notifications). This setting will be restored after 
> the process is finished. If the process is aborted, reset the level manually.