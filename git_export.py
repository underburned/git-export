import json
import os
from datetime import datetime
import gitlab
from gitlab import Gitlab
from loguru import logger
from pathlib import Path
import time
from typing import Optional
import subprocess


class GitExport:
    def __init__(self):
        self.datetime_suffix = datetime.now().strftime("%Y%m%d_%H%M%S")
        self.user_name = ""
        self.private_token = ""
        self.gl: Optional[Gitlab] = None
        self.file_name = 'token.txt'
        self.dir_path = Path("")
        self.export_group_timeout = 10
        self.export_group_meta = True
        self.export_project_meta = True
        self.export_project_archive = True
        self.export_wikis = True

    def read_credentials(self):
        with open(self.file_name, 'r') as f:
            for line in f.readlines():
                str_l = line.rstrip().split("=")
                if len(str_l) == 2:
                    if str_l[0] == "user_name":
                        self.user_name = str_l[1]
                    elif str_l[0] == "private_token":
                        self.private_token = str_l[1]

    def export_project_gitlab(self, id: int, path: Path):
        project = self.gl.projects.get(id)
        if self.export_project_meta:
            project_path = path / (project.path + ".zip")
            # Check access level
            access_level = 0
            if project.permissions['group_access'] is not None:
                access_level = project.permissions['group_access']['access_level']
            else:
                if project.permissions['project_access'] is not None:
                    access_level = project.permissions['project_access']['access_level']
            if access_level in [gitlab.const.MAINTAINER_ACCESS, gitlab.const.OWNER_ACCESS]:
                # Create the export
                export = project.exports.create()
                # Wait for the 'finished' status
                export.refresh()
                while export.export_status != 'finished':
                    time.sleep(1)
                    export.refresh()
                # Download the result
                with project_path.open('wb') as f:
                    export.download(streamed=True, action=f.write)
            else:
                logger.warning("access level is too low, skip")
        if self.export_project_archive:
            for branch in project.branches.list():
                branch_name = branch.name
                branch_name = branch_name.replace("\\", "-").replace("/", "-")
                project_path = path / (project.path + f"_{branch_name}.zip")
                zip = project.repository_archive(sha=branch.commit["id"], format='zip')
                with project_path.open('wb') as f:
                    f.write(zip)
        if self.export_wikis:
            if project.wiki_enabled:
                pages = project.wikis.list()
                wiki_path = path / (project.path + "-wiki")
                if len(pages) > 0:
                    wiki_path.mkdir(parents=True, exist_ok=True)
                for page in pages:
                    p = project.wikis.get(page.slug)
                    page_path = wiki_path / (p.slug + ".md")
                    with page_path.open('w', encoding="utf-8") as f:
                        f.write(p.content)

    def check_group_membership(self, group):
        user = self.gl.user
        members = group.members.list()
        res = False
        for member in members:
            if member.id == user.id:
                res = True
                break
        return res

    def export_group_gitlab(self, id: int, path: Path, name: str):
        # Create the export
        group = self.gl.groups.get(id)
        # Check access level
        access_level = 0
        if self.check_group_membership(group):
            member = group.members.get(self.gl.user.id)
            access_level = member.attributes['access_level']
            if access_level in [gitlab.const.MAINTAINER_ACCESS, gitlab.const.OWNER_ACCESS]:
                export = group.exports.create()

                # Wait for the export to finish
                time.sleep(self.export_group_timeout)

                group_path = path / (name + ".zip")

                # Download the result
                with group_path.open('wb') as f:
                    export.download(streamed=True, action=f.write)
            else:
                logger.warning("access level is too low, skip")
        else:
            logger.warning("current user is not a member of this group, skip")

    def export_gitlab(self):
        self.gl = Gitlab('https://gitlab.com', self.private_token)
        try:
            self.gl.auth()
        except gitlab.exceptions.GitlabAuthenticationError as e:
            logger.error(f"Authentication error: {e}, exit")
            return

        # Global notification settings
        # Turn off for metadata export
        settings = self.gl.notificationsettings.get()
        notification_level = settings.level
        settings.level = gitlab.const.NOTIFICATION_LEVEL_DISABLED
        settings.save()

        self.dir_path = self.dir_path / f"gitlab-backup-{self.user_name}-{self.datetime_suffix}"
        self.dir_path.mkdir(parents=True, exist_ok=True)

        # Personal projects
        personal_path = self.dir_path / self.gl.user.username
        personal_path.mkdir(parents=True, exist_ok=True)
        current_user = self.gl.users.get(self.gl.user.id)
        projects = current_user.projects.list(as_list=False)
        logger.info(f"Found {projects.total} personal projects.")
        for project in projects:
            logger.info(f" exporting \"{project.name}\"")
            self.export_project_gitlab(project.id, personal_path)

        # Groups
        groups = self.gl.groups.list(as_list=False)
        logger.info(f"Found {groups.total} groups total.")
        group_path = Path()
        for group in groups:
            if group.parent_id is not None:
                continue
            logger.info(f"Group \"{group.full_path}\":")
            group_path = self.dir_path / group.full_path
            group_path.mkdir(parents=True, exist_ok=True)
            if self.export_group_meta:
                logger.info(f" exporting group")
                self.export_group_gitlab(group.id, group_path, group.full_path)

            projects = group.projects.list(as_list=False)
            if projects.total > 0:
                logger.info(f" {projects.total} projects:")
            for project in projects:
                logger.info(f"  exporting \"{project.name}\"")
                self.export_project_gitlab(project.id, group_path)

            subgroups = group.subgroups.list(as_list=False)
            if subgroups.total > 0:
                logger.info(f" {subgroups.total} subgroups:")
                for subgroup in subgroups:
                    logger.info(f"  subgroup \"{subgroup.path}\":")
                    subgroup_path = self.dir_path / subgroup.full_path
                    subgroup_path.mkdir(parents=True, exist_ok=True)
                    subgroup = self.gl.groups.get(subgroup.id)
                    if self.export_group_meta:
                        logger.info(f"   exporting group")
                        self.export_group_gitlab(subgroup.id, subgroup_path, subgroup.path)

                    projects = subgroup.projects.list(as_list=False)
                    if projects.total > 0:
                        logger.info(f"   {projects.total} projects:")
                    for project in projects:
                        logger.info(f"    exporting \"{project.name}\"")
                        self.export_project_gitlab(project.id, subgroup_path)

        # Restore global notification settings
        settings.level = notification_level
        settings.save()


def main():
    ge = GitExport()
    ge.dir_path = Path("C:/Temp/GE")
    ge.read_credentials()
    ge.export_gitlab()


if __name__ == '__main__':
    main()
